<?php

namespace App\Http\Controllers;

use App\Models\Kota;
use App\Models\Provinsi;
use App\Models\SwapSourceApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SearchController extends Controller
{
    public function __construct(){
        $this->source = SwapSourceApi::pluck('source')->first();
        $this->apiKey = '0df6d5bf733214af6c6644eb8717c92c';
    }

    public function provinces(){
        if ($this->source == 'Database') {
            if (request()->id) {
                $result = Provinsi::where('province_id', request()->id)->get();
            } else {
                $result = Provinsi::all();
            }
        } elseif ($this->source == 'DirectApi') {
            $response = Http::withHeaders([
                'key' => $this->apiKey,
            ])->get('https://api.rajaongkir.com/starter/province', [
                'id' => request()->id,
            ]);

            $result = $response->json('rajaongkir.results');
        }


        return response()->json($result, 200);
    }
    public function cities(){
        if ($this->source == 'Database') {
            if (request()->id) {
                $result = Kota::where('city_id', request()->id)->get();
            }else{
                $result = Kota::all();
            }

        } elseif ($this->source == 'DirectApi') {
            $response = Http::withHeaders([
                'key' => $this->apiKey,
            ])->get('https://api.rajaongkir.com/starter/city', [
                'id' => request()->id,
            ]);

            $result = $response->json('rajaongkir.results');
        }

        return response()->json($result, 200);
    }
}
