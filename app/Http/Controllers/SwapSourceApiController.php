<?php

namespace App\Http\Controllers;

use App\Models\SwapSourceApi;
use Illuminate\Http\Request;

class SwapSourceApiController extends Controller
{
    public function index(){
        if (request()->source) {
            $source = SwapSourceApi::first();
            $source->source = request()->source;
            $source->save();

            return response()->json('Sukses mengganti source menjadi '.request()->source, 200);
        }
    }
}
