<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SwapSourceApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class,'login']);
Route::middleware('auth:sanctum')->group( function () {
    Route::get('search/provinces', [SearchController::class,'provinces']);
    Route::get('search/cities', [SearchController::class,'cities']);
    Route::get('swap-source', [SwapSourceApiController::class,'index']);
});
