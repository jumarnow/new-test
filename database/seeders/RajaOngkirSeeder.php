<?php

namespace Database\Seeders;

use App\Models\Kota;
use App\Models\Provinsi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class RajaOngkirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apiKey = '0df6d5bf733214af6c6644eb8717c92c';
        $endpoint = 'https://api.rajaongkir.com/starter/province';

        $response = Http::withHeaders([
            'key' => $apiKey,
        ])->get($endpoint);

        if ($response->successful()) {
            $provinces = $response->json()['rajaongkir']['results'];
            Provinsi::insert($provinces);
        } else {
            $this->command->info('Failed to fetch provinces from RajaOngkir API');
        }

        $endpoint = 'https://api.rajaongkir.com/starter/city';

        $response = Http::withHeaders([
            'key' => $apiKey,
        ])->get($endpoint);

        if ($response->successful()) {
            $cities = $response->json()['rajaongkir']['results'];
            Kota::insert($cities);
        } else {
            $this->command->info('Failed to fetch cities from RajaOngkir API');
        }

    }
}
