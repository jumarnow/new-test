<?php

namespace Database\Seeders;

use App\Models\SwapSourceApi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SwapSourceApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $swap = new SwapSourceApi();
        $swap->source = 'Database';
        $swap->save();
    }
}
