## Cara Install

1. Clone repository:

   ```shell
   git clone https://gitlab.com/jumarnow/new-test.git
   ```

2. Masuk ke direktori proyek:

   ```shell
   cd new-test
   ```

3. Install dependencies menggunakan Composer:

   ```shell
   composer install
   ```

4. Buat file `.env` dengan melakukan salinan dari file `.env.example`. Kemudian atur pengaturan database di dalam file `.env`.

5. Migrate database dan seeding:

   ```shell
   php artisan migrate --seed
   ```

6. Jalankan server lokal:

   ```shell
   php artisan serve
   ```

## Dokumentasi API

### Login

- Metode: POST
- URL: `/api/login`
- Body Request:
  ```json
  {
      "email": "admin@admin.com",
      "password": "123456"
  }
  ```

- Response:
  ```json
  {
      "message": "Login success",
      "access_token": "1|lspyVKSueY3gZvBHRKfFDqVFf1LzmYdMqwJ9NIys",
      "token_type": "Bearer"
  }
  ```
### Ambil Data Provinsi

- Metode: GET
- URL: `/api/search/provinces?id=1`
- Auth: `API Key`
- Response:
  ```json
    [
        {
            "id": 1,
            "province_id": 1,
            "province": "Bali",
            "created_at": null,
            "updated_at": null
        }
    ]
  ```
### Ambil Data Kota/Kabupaten

- Metode: GET
- URL: `/api/search/cities?id=1`
- Auth: `API Key`
- Response:
  ```json
    [
        {
            "id": 1,
            "city_id": 1,
            "province_id": 21,
            "province": "Nanggroe Aceh Darussalam (NAD)",
            "city_name": "Aceh Barat",
            "type": "Kabupaten",
            "postal_code": 23681,
            "created_at": null,
            "updated_at": null
        }
    ]
  ```
### Ganti Source Data

- Metode: GET
- Tipe: Database / DirectApi
- URL: `/api/swap-source/?source=DirectApi`
- Auth: `API Key`
- Response:
  ```json
    "Sukses mengganti source menjadi DirectApi"
  ```

